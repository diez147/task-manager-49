package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.LogDto;

public interface IJmsService {

    void send(@NotNull LogDto entity);

    LogDto createMessage(@NotNull Object object, @NotNull String type);

}

