package ru.tsc.babeshko.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.service.IJmsService;
import ru.tsc.babeshko.tm.dto.LogDto;
import ru.tsc.babeshko.tm.service.JmsService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JmsComponent {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final IJmsService service = new JmsService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LogDto logDto = service.createMessage(object, type);
            service.send(logDto);
        });
    }

    public void stop() {
        es.shutdown();
    }

}

