package ru.tsc.babeshko.tm.api.service.dto;

import ru.tsc.babeshko.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.babeshko.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {
}