package ru.tsc.babeshko.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

    void removeAllByUserId(@Nullable String userId);

}