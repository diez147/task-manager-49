package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.LogDto;

public interface ILoggerService {

    void writeLog(@NotNull LogDto message);

}